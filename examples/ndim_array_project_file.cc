/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <array>
#include <scalpel/scalpel.h>

int main()
{
    /* input data declarations */
    /* 2d array */
    std::array<std::array<int, 2>, 3> ddarr = {{{1, 2}, {3, 4}, {5, 6}}};
    /* 3d array */
    std::array<std::array<std::array<int, 2>, 2>, 2> dddarr = {
        {{{{{1, 2}}, {{3, 4}}}}, {{{{5, 6}}, {{7, 8}}}}}};

    scalpel::record_inputs(ddarr, dddarr);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 2; j++) {
            ddarr[i][j] += (i + 1) * (j + 1);
            if (i < 2) {
                for (int k = 0; k < 2; k++) {
                    dddarr[i][j][k] += (i + 1) * (j + 1) * (k + 1);
                }
            }
        }
    }

    scalpel::record_outputs(ddarr, dddarr);

    return 0;
}
