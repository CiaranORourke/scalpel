/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <scalpel/scalpel.h>
#include <set>

int main()
{
    std::set<int> ivec;
    std::set<float> fvec;
    std::set<double> dvec;

    /* making primary insision */
    scalpel::replay_inputs(ivec, fvec, dvec);

    ivec.insert(5);
    fvec.insert(5.5);
    dvec.insert(5.5);

    scalpel::compare_outputs(ivec, fvec, dvec);

    return 0;
}
