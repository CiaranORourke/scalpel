# Examples

## Running the examples

The example executables categorise the different functionalities of the Scalpel project. To test one of the sample codes (in the `[build-dir]/examples` directory, after building to project), run;

    # Use Scalpel functions in the original codebase.
    ./*project_file

    # Use Scalpel functions in separate file for optimising code block.
    ./*scalpel_file

Variables are written to specified '.dat' files in the data subdirectory.
