/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <scalpel/scalpel.h>

int main()
{
    /* input data declarations */
    /* character types */
    char a = 'a';

    /* integer types */
    int w           = 10;
    short int x     = 20;
    long int y      = 0;
    long long int z = 10;

    /* floating point types */
    float theta     = 1.5;
    double phi      = 2.545;
    long double psi = 3.14;

    /* write project inputs to file */
    scalpel::record_inputs(a, w, x, y, z, theta, phi, psi);
    /* write project inputs to human-readable file */
    scalpel::view_inputs(a, w, x, y, z, theta, phi, psi);

    for (int i = 0; i < 20; i++) {
        y %= 10;
        for (int j = 0; j < 10; j++) {
            w += y / 2;
            w %= 30;
            x += y;
            x %= 10;
            z += w - x;
            theta += x;
            phi += x;
            theta = std::fmod(theta, 3.14);
            phi   = std::fmod(phi, 3.14);
            psi += x;
            psi = std::fmod(psi, 3.14);
        }
        a++;
        y++;
    }

    /* write project outputs to file */
    scalpel::record_outputs(a, w, x, y, z, theta, phi, psi);
    /* write project outputs to human-readable file */
    scalpel::view_outputs(a, w, x, y, z, theta, phi, psi);

    return 0;
}
