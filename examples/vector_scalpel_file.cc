/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <scalpel/scalpel.h>
#include <vector>

int main()
{
    std::vector<int> ivec;
    std::vector<float> fvec;
    std::vector<double> dvec;

    /* making primary insision */
    scalpel::replay_inputs(ivec, fvec, dvec);

    for (std::vector<int>::size_type i = 0; i < ivec.size(); i++) {
        ivec[i] += i;
        fvec[i] += 1.5 * i;
        dvec[i] += 2.5 * i;
    }

    scalpel::compare_outputs(ivec, fvec, dvec);

    return 0;
}
