/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <scalpel/scalpel.h>
#include <vector>

int main()
{
    /* input data declarations */
    std::vector<int> ivec    = {1, 2, 3, 4};
    std::vector<float> fvec  = {1.5, 2.5, 3.5, 4.5};
    std::vector<double> dvec = {1.5, 2.5, 3.5, 4.5};

    /* making primary insision */
    scalpel::record_inputs(ivec, fvec, dvec);

    for (std::vector<int>::size_type i = 0; i < ivec.size(); i++) {
        ivec[i] += i;
        fvec[i] += 1.5 * i;
        dvec[i] += 2.5 * i;
    }

    scalpel::record_outputs(ivec, fvec, dvec);

    return 0;
}
