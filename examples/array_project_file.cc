/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <array>
#include <scalpel/scalpel.h>

int main()
{
    /* input data declarations */
    std::array<int, 4> iarr    = {1, 2, 3, 4};
    std::array<float, 4> farr  = {1.5, 2.5, 3.5, 4.5};
    std::array<double, 4> darr = {1.5, 2.5, 3.5, 4.5};

    /* making primary insision */
    scalpel::record_inputs(iarr, farr, darr);

    for (int i = 0; i < 4; i++) {
        iarr[i] += i;
        farr[i] += 1.5 * i;
        darr[i] += 2.5 * i;
    }

    scalpel::record_outputs(iarr, farr, darr);

    return 0;
}
