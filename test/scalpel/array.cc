/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <array>
#include <catch2/catch.hpp>
#include <cmath>
#include <scalpel/scalpel.h>
#include <vector>

TEMPLATE_LIST_TEST_CASE(
    "basic arrays can be streamed", "[array][type][stream]", Fundamental_types)
{
    GIVEN("an input array")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));

        std::array<TestType, Scalpel_MAX_CONTAINER_SIZE> project_variable;

        std::move(
            random_values.begin(), random_values.end(),
            project_variable.begin());

        WHEN("the array is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            std::array<TestType, Scalpel_MAX_CONTAINER_SIZE> scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);

            THEN("the replayed array equals the recorded array")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}

TEMPLATE_LIST_TEST_CASE(
    "2D arrays can be streamed", "[array][type][stream]", Fundamental_types)
{
    constexpr int dim_size = compile_time_root<2>(Scalpel_MAX_CONTAINER_SIZE);
    GIVEN("a 2D array as an input")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));
        std::array<std::array<TestType, dim_size>, dim_size> project_variable;

        auto rv_it = random_values.begin();
        for (auto& row : project_variable) {
            std::move(rv_it, std::next(rv_it, dim_size), row.begin());
            std::advance(rv_it, dim_size);
        }

        WHEN("the array is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            decltype(project_variable) scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the recorded array equals the replayed array")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}

TEMPLATE_LIST_TEST_CASE(
    "3D arrays can be streamed", "[array][type][stream]", Fundamental_types)
{
    constexpr int dim_size = compile_time_root<3>(Scalpel_MAX_CONTAINER_SIZE);
    GIVEN("a 3D array as an input")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));
        std::array<
            std::array<std::array<TestType, dim_size>, dim_size>, dim_size>
            project_variable;

        auto rv_it = random_values.begin();
        for (auto& d0 : project_variable) {
            for (auto& d1 : d0) {
                std::move(rv_it, std::next(rv_it, dim_size), d1.begin());
                std::advance(rv_it, dim_size);
            }
        }

        WHEN("the array is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            decltype(project_variable) scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the recorded array equals the replayed array")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}
