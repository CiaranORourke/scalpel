/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <catch2/catch.hpp>
#include <deque>
#include <scalpel/scalpel.h>
#include <vector>

TEMPLATE_LIST_TEST_CASE(
    "deques can be streamed", "[deque][type][stream]", Fundamental_types)
{
    std::cout << "before given" << std::endl;
    GIVEN("an input deque")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));

        std::deque<TestType> project_variable(
            random_values.begin(), random_values.end());

        WHEN("the deque is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            std::deque<TestType> scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);

            THEN("the replayed deque equals the recorded deque")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}
