/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <catch2/catch.hpp>
#include <scalpel/scalpel.h>

TEMPLATE_LIST_TEST_CASE(
    "binary and human-readable IO function correctly",
    "[fundamental][type][io]",
    Fundamental_types)
{
    const TestType project_input_variable = GENERATE(take(
        Scalpel_NUM_SAMPLES, random(
                                 std::numeric_limits<TestType>::min(),
                                 std::numeric_limits<TestType>::max())));

    const TestType project_output_variable = GENERATE(take(
        Scalpel_NUM_SAMPLES, random(
                                 std::numeric_limits<TestType>::min(),
                                 std::numeric_limits<TestType>::max())));

    WHEN("input/output are written to file in binary format")
    {
        scalpel::record_inputs(project_input_variable);
        scalpel::record_outputs(project_output_variable);
    }
    WHEN("input/output are written to file in human-readable format")
    {
        scalpel::view_inputs(project_input_variable);
        scalpel::view_inputs(project_output_variable);
    }
    WHEN("input/output are written to stdout")
    {
        scalpel::view_inputs(std::cout, project_input_variable);
        scalpel::view_outputs(std::cout, project_output_variable);
    }
    WHEN("input is read from binary formatted file")
    {
        TestType scalpel_input_variable;
        scalpel::replay_inputs(scalpel_input_variable);
        THEN("the replayed input equals the recorded input")
        {
            REQUIRE(project_input_variable == scalpel_input_variable);
        }
    }
    WHEN("outputs are compared to those in binary formatted file")
    {
        THEN("equality validation succeeds")
        {
            REQUIRE(scalpel::compare_outputs(project_output_variable));
        }
    }
}
