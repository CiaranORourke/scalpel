/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include <catch2/catch.hpp>
#include <scalpel/mpi.h>
#include <scalpel/scalpel.h>

SCENARIO("MPI processes stream using separate files", "[mpi]")
{
    std::cout << "initialising MPI" << std::endl;
    MPI_Init(NULL, NULL);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    scalpel::record_outputs(MPI_COMM_WORLD, rank);
    scalpel::view_outputs(MPI_COMM_WORLD, rank);

    REQUIRE(scalpel::compare_outputs(MPI_COMM_WORLD, rank));

    MPI_Finalize();
}
