#include <tuple>

#define Scalpel_NUM_SAMPLES 10
#define Scalpel_MAX_CONTAINER_SIZE 100

using Fundamental_types = std::tuple<
    char,
    signed char,
    unsigned char,
    wchar_t,
    char16_t,
    char32_t,
    short,
    unsigned short,
    int,
    unsigned int,
    long,
    unsigned long,
    long long,
    unsigned long long,
    float,
    double,
    long double>;

template<std::size_t NumDimensions>
constexpr unsigned int compile_time_root(unsigned int x)
{
    unsigned int low = 0, high = x;

    do {
        if (low == high) {
            return low;
        }

        const unsigned int mid = low + (high - low) / 2;

        unsigned int mid_root = x;
        for (std::size_t i = 1; i < NumDimensions; ++i) {
            mid_root /= mid;
        }

        if (mid_root < mid) {
            high = mid - 1;
        }
        else {
            low = mid + 1;
        }
    } while (true);
}
