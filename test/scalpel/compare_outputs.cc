/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <catch2/catch.hpp>
#include <numeric>
#include <scalpel/scalpel.h>
#include <string>

TEMPLATE_LIST_TEST_CASE(
    "fundamental data types can be compared for equality",
    "[fundamental][type][compare]",
    Fundamental_types)
{
    GIVEN("an output variable")
    {
        const TestType project_variable = GENERATE(take(
            Scalpel_NUM_SAMPLES, random(
                                     std::numeric_limits<TestType>::min(),
                                     std::numeric_limits<TestType>::max())));
        WHEN(
            "the output variable is recorded and compared to the recorded variable")
        {
            scalpel::record_outputs(project_variable);
            THEN("equality validation succeeds")
            {
                REQUIRE(scalpel::compare_outputs(project_variable));
            }
        }
        GIVEN("a distinct variable as an output")
        {
            const TestType scalpel_variable = GENERATE_REF(take(  // NOLINT
                1, filter(
                       [&project_variable](TestType var) {
                           return var != project_variable;
                       },
                       random(
                           std::numeric_limits<TestType>::min(),
                           std::numeric_limits<TestType>::max()))));
            WHEN("the output variable is compared to the recorded variable")
            {
                THEN("equality validation fails")
                {
                    REQUIRE(!scalpel::compare_outputs(scalpel_variable));
                }
            }
        }
    }
}

/* special case bool */
TEST_CASE(
    "fundamental data types can be compared for equality",
    "[fundamental][type][compare]")
{
    GIVEN("an output variable")
    {
        const bool project_variable = GENERATE(false, true);
        std::cout << "first given" << project_variable << std::endl;
        WHEN(
            "the output variable is recorded and compared to the recorded variable")
        {
            std::cout << "first when" << project_variable << std::endl;
            scalpel::record_outputs(project_variable);

            THEN("equality validation succeeds")
            {
                std::cout << "first then" << project_variable << std::endl;
                REQUIRE(scalpel::compare_outputs(project_variable));
            }
        }
        GIVEN("a distinct variable as an output")
        {
            std::cout << "second given" << project_variable << std::endl;
            const bool scalpel_variable = !project_variable;
            WHEN("the output variable is compared to the recorded variable")
            {
                std::cout << "second when" << project_variable << std::endl;
                THEN("equality validation fails")
                {
                    std::cout << "second then" << project_variable << std::endl;
                    REQUIRE(!scalpel::compare_outputs(scalpel_variable));
                }
            }
        }
    }
}
