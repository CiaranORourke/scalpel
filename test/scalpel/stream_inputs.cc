/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <catch2/catch.hpp>
#include <numeric>
#include <scalpel/scalpel.h>

TEMPLATE_LIST_TEST_CASE(
    "fundamental data types can be streamed",
    "[fundamental][type][stream]",
    Fundamental_types)
{
    GIVEN("an input variable")
    {
        const TestType project_variable = GENERATE(take(
            Scalpel_NUM_SAMPLES, random(
                                     std::numeric_limits<TestType>::min(),
                                     std::numeric_limits<TestType>::max())));
        WHEN("the input variable is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            TestType scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the replayed variable equals the recorded variable")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}

/* special case bool */
TEST_CASE(
    "fundamental data types can be streamed", "[fundamental][type][stream]")
{
    GIVEN("an input variable")
    {
        const bool project_variable = GENERATE(false, true);
        WHEN("the input variable is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            bool scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the replayed variable equals the recorded variable")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}
