/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <catch2/catch.hpp>
#include <cmath>
#include <scalpel/scalpel.h>
#include <vector>

TEMPLATE_LIST_TEST_CASE(
    "basic vectors can be streamed",
    "[vector][type][stream]",
    Fundamental_types)
{
    GIVEN("an input vector")
    {
        std::vector<TestType> project_variable = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));

        WHEN("the vector is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            std::vector<TestType> scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);

            THEN("the replayed vector equals the recorded vector")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}

TEMPLATE_LIST_TEST_CASE(
    "2D vectors can be streamed", "[vector][type][stream]", Fundamental_types)
{
    constexpr int dim_size = compile_time_root<2>(Scalpel_MAX_CONTAINER_SIZE);
    GIVEN("a 2D vector as an input")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));
        std::vector<std::vector<TestType>> project_variable(dim_size);

        auto rv_it = random_values.begin();
        for (auto& row : project_variable) {
            std::move(
                rv_it, std::next(rv_it, dim_size), std::back_inserter(row));
            std::advance(rv_it, dim_size);
        }

        WHEN("the vector is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            decltype(project_variable) scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the recorded vector equals the replayed vector")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}

TEMPLATE_LIST_TEST_CASE(
    "3D vectors can be streamed", "[vector][type][stream]", Fundamental_types)
{
    constexpr int dim_size = compile_time_root<3>(Scalpel_MAX_CONTAINER_SIZE);
    GIVEN("a 3D vector as an input")
    {
        std::vector<TestType> random_values = GENERATE(chunk(
            Scalpel_MAX_CONTAINER_SIZE,
            take(
                Scalpel_MAX_CONTAINER_SIZE,
                random(
                    std::numeric_limits<TestType>::min(),
                    std::numeric_limits<TestType>::max()))));
        std::vector<std::vector<std::vector<TestType>>> project_variable(
            dim_size, std::vector<std::vector<TestType>>(dim_size));

        auto rv_it = random_values.begin();
        for (auto& d0 : project_variable) {
            for (auto& d1 : d0) {
                std::move(
                    rv_it, std::next(rv_it, dim_size), std::back_inserter(d1));
                std::advance(rv_it, dim_size);
            }
        }

        WHEN("the vector is recorded and replayed")
        {
            scalpel::record_inputs(project_variable);

            decltype(project_variable) scalpel_variable;
            scalpel::replay_inputs(scalpel_variable);
            THEN("the recorded vector equals the replayed vector")
            {
                REQUIRE(project_variable == scalpel_variable);
            }
        }
    }
}
