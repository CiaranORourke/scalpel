/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_UTILS
#define Scalpel_UTILS 1

#include <array>
#include <cmath>
#include <iostream>
#include <vector>

namespace scalpel {

template<
    typename Int,
    std::enable_if_t<std::is_integral_v<std::remove_reference_t<Int>>>* =
        nullptr>
inline bool compare_to(const Int& a, const Int& b);

template<
    typename Float,
    std::enable_if_t<
        std::is_floating_point_v<std::remove_reference_t<Float>>>* = nullptr>
inline bool compare_to(const Float& a, const Float& b);

/* Printing functions for debugging */
template<typename T>
inline void print_variable(const T& var);

template<typename T, std::size_t Size>
inline void print_variable(const std::array<T, Size>& var);

template<typename T, std::size_t Size, std::size_t NextSize>
inline void print_variable(
    const std::array<std::array<T, NextSize>, Size>& var);

template<typename T>
inline void print_variable(const std::vector<T>& var);

template<typename T>
inline void print_variable(const std::vector<std::vector<T>>& var);

template<typename T>
inline void print_object(const T& var);

template<typename T>
void print_arguments(const T& var);

template<typename T, typename... Args>
void print_arguments(const T& var, const Args... args);
}  // namespace scalpel

/* default to == operator */
template<
    typename T,
    std::enable_if_t<std::is_integral_v<std::remove_reference_t<T>>>*>
bool scalpel::compare_to(const T& a, const T& b)
{
    return (a == b);
}

template<
    typename Float,
    std::enable_if_t<std::is_floating_point_v<std::remove_reference_t<Float>>>*>
bool scalpel::compare_to(const Float& a, const Float& b)
{
    return (std::fabs(a - b) < a * 0.001);
}

/* Debugging print functions */
/* Fundamental types */
template<typename T>
void scalpel::print_variable(const T& var)
{
    std::cout << var;
}

/* Array printing */
template<typename T, std::size_t Size>
void scalpel::print_variable(const std::array<T, Size>& var)
{
    for (int i = 0; i < Size; i++) {
        print_variable(var[i]);
        /* delimit elements */
        std::cout << " ";
    }
}

template<typename T, std::size_t Size, std::size_t NextSize>
void scalpel::print_variable(
    const std::array<std::array<T, NextSize>, Size>& var)
{
    for (int i = 0; i < Size; i++) {
        print_variable(var[i]);
    }
}

/* Vector printing */
template<typename T>
void scalpel::print_variable(const std::vector<T>& var)
{
    for (int i = 0; i < var.size(); i++) {
        print_variable(var[i]);
        /* delimit elements */
        std::cout << " ";
    }
}

template<typename T>
void scalpel::print_variable(const std::vector<std::vector<T>>& var)
{
    for (int i = 0; i < var.size(); i++) {
        print_variable(var[i]);
    }
}

template<typename T>
void scalpel::print_object(const T& var)
{
    print_variable(var);
    /* endline between objects */
    std::cout << '\n';
}

template<typename T>
void scalpel::print_arguments(const T& var)
{
    std::cout << "===========================\n";
    print_object(var);
    std::cout << "===========================\n";
}

template<typename T, typename... Args>
void scalpel::print_arguments(const T& var, const Args... args)
{
    std::cout << "===========================\n";
    print_object(var);
    print_arguments(args...);
}

#endif  // Scalpel_UTILS
