/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_PARSE_VARIABLES
#define Scalpel_PARSE_VARIABLES 1

#include <scalpel/process_variable.h>

namespace scalpel {

/* functions for parsing arguments one by one */
template<bool Alternate, typename Stream, typename T, typename... Args>
void isolate_next_variable(records& log, Stream&& file, T&& var);

template<bool Alternate, typename Stream, typename T, typename... Args>
void isolate_next_variable2(records& log, Stream&& file, T&& var);

template<bool Alternate, typename Stream, typename... Args>
inline void parse_variables(records& log, Stream&& file, Args&&... args);

template<bool Alternate, typename Stream, typename T, typename... Args>
void isolate_next_variable(
    records& log, Stream&& file, T&& var, Args&&... args);

/* wrappers for reading, writing, and comparing functions */
template<bool IsTextfile, typename... Args>
inline void serialize(records& log, std::ostream& stream, Args&&... args);

template<bool Compare, typename... Args>
inline void deserialize(records& log, std::istream& stream, Args&&... args);

}  // namespace scalpel

/*
 * Used to pull next variable from argument list so that we can tell
 * process_variable the type
 */
template<bool Alternate, typename Stream, typename T, typename... Args>
void scalpel::isolate_next_variable(
    scalpel::records& log, Stream&& file, T&& var)
{
    log.add_comment(
        "Variable " + std::to_string(log.get_num_variables()) + ": ");
    log.reset_fail();
    process_variable<Alternate>(
        log, std::forward<Stream>(file), std::forward<T>(var));
    log.increment_variables();
}

template<bool Alternate, typename Stream, typename T, typename... Args>
void scalpel::isolate_next_variable(
    scalpel::records& log, Stream&& file, T&& var, Args&&... args)
{
    log.add_comment(
        "Variable " + std::to_string(log.get_num_variables()) + ": ");
    log.reset_fail();
    process_variable<Alternate>(
        log, std::forward<Stream>(file), std::forward<T>(var));
    log.increment_variables();

    isolate_next_variable<Alternate>(
        log, std::forward<Stream>(file), std::forward<Args>(args)...);
}

template<bool Alternate, typename Stream, typename... Args>
void scalpel::parse_variables(
    scalpel::records& log, Stream&& file, Args&&... args)
{
    isolate_next_variable<Alternate>(
        log, std::forward<Stream>(file), std::forward<Args>(args)...);
    log.print_status(std::cout);
    // log.print_comments(std::cout);
}
/*
 * open file for writing input/output variables
 * keep file open while parsing variables using process_variable
 */
template<bool IsTextfile, typename... Args>
void scalpel::serialize(
    scalpel::records& log, std::ostream& stream, Args&&... args)
{
    /* is_textfile for !std::ios::binary */
    parse_variables<IsTextfile>(log, stream, std::forward<Args>(args)...);
}

/*
 * open file for reading input variables
 * keep file open while parsing variables using parse_next_variable
 */
template<bool Compare, typename... Args>
void scalpel::deserialize(
    scalpel::records& log, std::istream& stream, Args&&... args)
{
    parse_variables<Compare>(log, stream, std::forward<Args>(args)...);
}


#endif  // Scalpel_PARSE_VARIABLES
