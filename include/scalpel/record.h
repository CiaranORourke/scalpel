/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_RECORD
#define Scalpel_RECORD 1

#include <ostream>
#include <string>
#include <vector>

namespace scalpel {
class records;
}

class scalpel::records {
  public:
    records() :
        m_num_variables(0),
        m_num_datums(0),
        m_num_fails(0),
        m_num_passes(0),
        m_failed(false){};

    unsigned int get_num_variables() const { return m_num_variables; };
    unsigned int get_num_datums() const { return m_num_datums; };
    unsigned int get_num_fails() const { return m_num_fails; };
    unsigned int get_num_passes() const { return m_num_passes; };
    bool is_fail() const { return m_failed; };

    void set_fail() { m_failed = true; };
    void reset_fail() { m_failed = false; };

    void increment_variables() { ++m_num_variables; };
    void increment_datums() { ++m_num_datums; };
    void increment_fails() { ++m_num_fails; };
    void increment_passes() { ++m_num_passes; };

    void add_comment(std::string comment) { m_comments.push_back(comment); }
    void add_comment(unsigned int i, std::string comment)
    {
        m_comments[i].append(comment);
    }

    void print_status(std::ostream& os) const
    {
        os << "================ Status ================\n";
        os << "Number of variables processed: " << get_num_variables() << '\n';
        os << "Number of fundamental datums processed: " << get_num_datums()
           << '\n';
        os << "Number of variables failed: " << get_num_fails() << '\n';
    }

    void print_comments(std::ostream& os) const
    {
        os << "=============== Comments ===============\n";
        for (std::vector<std::string>::size_type i = 0; i < m_comments.size();
             i++) {
            os << m_comments[i] << '\n';
        }
    }

  private:
    unsigned int m_num_variables;
    unsigned int m_num_datums;
    unsigned int m_num_fails;
    unsigned int m_num_passes;

    bool m_failed;

    std::vector<std::string> m_comments;
};

#endif  // Scalpel_RECORD
