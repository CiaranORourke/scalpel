/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_BINARY_IO
#define Scalpel_BINARY_IO 1

#include <bitset>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <type_traits>

namespace scalpel {

/* function for writing a variable to binary file */
template<typename T>
void stream_variable(std::ostream& write_file, const T& var);


/* function for reading a variable from binary file */
template<typename T>
void stream_variable(std::istream& read_file, T& var);


/* functions to supply appropriate std::uint_t type for streamed variables */
template<std::size_t Size>
class int_for_size;

template<>
class int_for_size<1>;

template<>
class int_for_size<2>;

template<>
class int_for_size<4>;

template<>
class int_for_size<8>;

template<>
class int_for_size<16>;

template<typename T>
class int_for_type;
}  // namespace scalpel

/*
 * if this is compiled the supplied variable has an incorrect size
 * this means the data type is unsupported. i.e the scalpel library could not
 * parse the argument into fundamental data types
 */
template<std::size_t Size>
class scalpel::int_for_size {
    static_assert(
        Size != Size,
        "Unsupported data type. For STL container support, include the scalpel/containers.h header file");
};

/* define type with std::uint_t of the same size of the input parameter */
template<>
class scalpel::int_for_size<1> {
  public:
    using type = std::uint8_t;
};

template<>
class scalpel::int_for_size<2> {
  public:
    using type = std::uint16_t;
};

template<>
class scalpel::int_for_size<4> {
  public:
    using type = std::uint32_t;
};

template<>
class scalpel::int_for_size<8> {
  public:
    using type = std::uint64_t;
};

template<>
class scalpel::int_for_size<16> {
  public:
    using type = std::uint64_t;
};

template<typename T>
class scalpel::int_for_type {
  public:
    using type = typename int_for_size<sizeof(T)>::type;
};

static_assert(
    std::is_same<scalpel::int_for_type<char[1]>::type, std::uint8_t>::value,
    "");
static_assert(
    std::is_same<scalpel::int_for_type<char[2]>::type, std::uint16_t>::value,
    "");
static_assert(
    std::is_same<scalpel::int_for_type<char[4]>::type, std::uint32_t>::value,
    "");
static_assert(
    std::is_same<scalpel::int_for_type<char[8]>::type, std::uint64_t>::value,
    "");

/* binary write of template type */
template<typename T>
void scalpel::stream_variable(std::ostream& write_file, const T& var)
{
    const int num_bytes = sizeof(T);
    std::uint8_t bytes[num_bytes];

    if (num_bytes <= 8) {

        /* Define a std::uint*_t variable that matches the size of the data type
         * T */
        using bit_t = typename int_for_type<T>::type;
        bit_t bits;

        memcpy(&bits, &var, num_bytes);

        for (int i = 0; i < num_bytes; ++i) {
            bytes[i] = static_cast<std::uint8_t>((bits >> 8 * i) & 0xFF);
        }
    }
    else {
        /* Define a std::uint64_t array that matches the size of the data type T
         */
        const int num_words = sizeof(T) / 8;
        using bit_t         = typename int_for_size<8>::type;
        bit_t bits[num_words];

        memcpy(bits, &var, num_bytes);

        for (int j = 0; j < num_words; ++j) {
            for (int i = 0; i < 8; ++i) {
                bytes[j * 8 + i] =
                    static_cast<std::uint8_t>((bits[j] >> 8 * i) & 0xFF);
            }
        }
    }
    write_file.write(reinterpret_cast<char*>(bytes), num_bytes);
}

/* binary read of template type */
template<typename T>
void scalpel::stream_variable(std::istream& read_file, T& var)
{
    const int num_bytes = sizeof(T);
    std::uint8_t bytes[num_bytes];

    read_file.read(reinterpret_cast<char*>(bytes), num_bytes);

    if (num_bytes <= 8) {
        /* Define a std::uint*_t variable that matches the size of the data type
         * T */
        using bit_t = typename int_for_type<T>::type;
        bit_t bits  = 0;

        for (int i = 0; i < num_bytes; ++i) {
            bits += (static_cast<bit_t>(bytes[i]) << 8 * i);
        }

        memcpy(&var, &bits, num_bytes);
    }
    else {
        /* Define a std::uint64_t array that matches the size of the data type T
         */
        const int num_words = num_bytes / 8;
        using bit_t         = typename int_for_size<8>::type;
        bit_t bits[num_words];

        for (int j = 0; j < num_words; ++j) {
            bits[j] = 0;
            for (int i = 0; i < 8; ++i) {
                bits[j] += (static_cast<bit_t>(bytes[j * 8 + i]) << 8 * i);
            }
        }

        memcpy(&var, bits, num_bytes);
    }
}

#endif  // Scalpel_BINARY_IO
