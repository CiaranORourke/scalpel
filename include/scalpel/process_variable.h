/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_PROCESS_VARIABLE
#define Scalpel_PROCESS_VARIABLE 1

#include <array>
#include <scalpel/binary_io.h>
#include <scalpel/record.h>
#include <scalpel/utils.h>

namespace scalpel {

/*
 * Alternate parameter used for specialisation below in order to choose between
 * stream_variable and alternate_stream_variable, and to switch behaviour of
 * read overload of process_variable
 */
template<bool Alternate, typename Stream, typename T>
struct choose;

template<typename Stream, typename T>
struct choose<false, Stream, T>;

template<typename Stream, typename T>
struct choose<true, Stream, T>;

/*
 * determine if type is an iterable container
 * checks for begin and end iterators
 */
template<typename, typename = void>
constexpr bool is_end_insertable{};

template<typename T>
constexpr bool is_end_insertable<
    T,
    std::void_t<
        decltype(std::declval<T>().get_allocator()),
        decltype(std::declval<T>().begin()),
        decltype(std::declval<T>().end())>> = true;

/*
 * choose how to stream variable based on whether it is a fundamental
 * (arithmetic) type or a particular class of container
 */
template<
    bool Alternate,
    typename Stream,
    typename T,
    std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>>* =
        nullptr>
inline void process_variable(records& log, Stream&& file, T&& var);

/*
 * Handle containers that are iterable and insertable
 * Separate overloads for streaming to and from file due to difference in
 * handling size
 */
template<
    bool Alternate,
    typename Container,
    std::enable_if_t<scalpel::is_end_insertable<Container>>* = nullptr>
inline void process_variable(
    records& log, std::ostream& file, const Container& var);

template<
    bool Alternate,
    typename Container,
    std::enable_if_t<scalpel::is_end_insertable<Container>>* = nullptr>
inline void process_variable(records& log, std::istream& file, Container& var);

/* std::array handled explicitly due to static, compile-time size */
template<bool Alternate, typename Stream, typename T, std::size_t Size>
inline void process_variable(
    records& log, Stream&& file, std::array<T, Size>& var);

/* functions for writing variables to a human readable file */
template<typename T>
inline void alternate_stream_variable(
    records& log, std::ostream& write_file, T&& var);

template<typename T>
inline void stream_variable_human_readable(
    records& log, std::ostream& write_file, const T& var);

/* function for comparing variable to that on file */
template<typename T>
inline void alternate_stream_variable(
    records& log, std::istream& read_file, const T& var);

}  // namespace scalpel

template<typename Stream, typename T>
struct scalpel::choose<false, Stream, T> {
    static void stream_variable(scalpel::records& log, Stream&& stream, T&& var)
    {
        scalpel::stream_variable(
            std::forward<Stream>(stream), std::forward<T>(var));
        log.increment_datums();
    }

    static void process_variable(scalpel::records& log, Stream& stream, T& var)
    {
        typename T::size_type size;
        scalpel::process_variable<false>(log, stream, size);

        for (auto i = 0; i < size; ++i) {
            typename T::value_type v;
            scalpel::process_variable<false>(log, stream, v);
            var.insert(
                var.end(), v); /* supported for all iterable containers */
        }
    }
};

template<typename Stream, typename T>
struct scalpel::choose<true, Stream, T> {
    static void stream_variable(scalpel::records& log, Stream&& stream, T&& var)
    {
        scalpel::alternate_stream_variable(
            log, std::forward<Stream>(stream), std::forward<T>(var));
        log.increment_datums();
    }

    /* reduces to comparison */
    static void process_variable(scalpel::records& log, Stream& stream, T& var)
    {
        scalpel::process_variable<true>(log, stream, var.size());

        for (const auto& v : var) {
            scalpel::process_variable<true>(log, stream, v);
        }
    }
};

/*
 * variable processing for fundamental datatypes
 * complex streaming (below) reduces to this
 */
template<
    bool Alternate,
    typename Stream,
    typename T,
    std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>>*>
void scalpel::process_variable(scalpel::records& log, Stream&& file, T&& var)
{
    choose<Alternate, Stream, T>::stream_variable(
        log, std::forward<Stream>(file), std::forward<T>(var));
}

/*
 * Overloads for container support
 * Argument overload recursion allows for isolation of containers of unknown
 * element type
 * Each isolated variable is put through the same recursion. Once a fundamental
 * data type is reached, the usual variable parsing functionality is triggered
 */

/*
 * Specialised overload for std::array due to static size
 */
template<bool Alternate, typename Stream, typename T, std::size_t Size>
void scalpel::process_variable(
    scalpel::records& log, Stream&& file, std::array<T, Size>& var)
{
    for (auto& v : var) {
        process_variable<Alternate>(log, std::forward<Stream>(file), v);
    }
}

/*
 * Write size so reading counterpart knows the number of elements to stream,
 * Then handle elements individually (potentially recursive)
 */
template<
    bool Alternate,
    typename Container,
    std::enable_if_t<scalpel::is_end_insertable<Container>>*>
void scalpel::process_variable(
    scalpel::records& log, std::ostream& file, const Container& var)
{
    process_variable<Alternate>(log, file, var.size());

    for (const auto& v : var) {
        process_variable<Alternate>(log, file, v);
    }
}

/*
 * Read size first (written by counterpart function
 * Then handle elements individually (potentially recursive)
 */
template<
    bool Alternate,
    typename Container,
    std::enable_if_t<scalpel::is_end_insertable<Container>>*>
void scalpel::process_variable(
    scalpel::records& log, std::istream& file, Container& var)
{
    scalpel::choose<Alternate, std::istream, Container>::process_variable(
        log, file, var);
}

/* write variable of unknown type to text file */
template<typename T>
void scalpel::stream_variable_human_readable(
    scalpel::records& log, std::ostream& write_file, const T& var)
{
    if (write_file << var << " ") {
        log.add_comment(log.get_num_variables(), "Variable written. ");
    }
    else {
        if (!log.is_fail()) {
            log.set_fail();
        }
        log.increment_fails();
        log.add_comment(log.get_num_variables(), "Variable not written. ");
    }
}

template<typename T>
void scalpel::alternate_stream_variable(
    scalpel::records& log, std::ostream& write_file, T&& var)
{
    stream_variable_human_readable(log, write_file, std::forward<T>(var));

    /* endline to separate varibles */
    write_file << std::endl;
}

template<typename T>
void scalpel::alternate_stream_variable(
    scalpel::records& log, std::istream& read_file, const T& var)
{
    T old_var;

    stream_variable(read_file, old_var);

    if (!compare_to(old_var, var)) {
        if (!log.is_fail()) {
            log.add_comment(
                log.get_num_variables(), "Parameters don't match: ");
            log.set_fail();
        }
        log.increment_fails();
        log.add_comment(
            log.get_num_variables(), "(" + std::to_string(var) + " != "
                                         + std::to_string(old_var) + ") ");
    }
}

#endif  // Scalpel_PROCESS_VARIABLE
