/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_MPI
#define Scalpel_MPI 1

#include <mpi.h>
#include <scalpel/scalpel.h>
#include <string>

namespace scalpel {

std::string unique_filename_for_process(
    MPI_Comm&& comm, std::string&& filename);

/* overloaded functions for MPI support */
template<typename... Args>
void record_inputs(MPI_Comm&& comm, Args&&... args);

template<typename... Args>
void replay_inputs(MPI_Comm&& comm, Args&&... args);

template<typename... Args>
void record_outputs(MPI_Comm&& comm, Args&&... args);

template<typename... Args>
bool compare_outputs(MPI_Comm&& comm, Args&&... args);

template<typename... Args>
void view_inputs(MPI_Comm&& comm, Args&&... args);

template<typename... Args>
void view_outputs(MPI_Comm&& comm, Args&&... args);

}  // namespace scalpel

/* MPI SUPPORT */

std::string scalpel::unique_filename_for_process(
    MPI_Comm&& comm, std::string&& filename)
{
    /* MPI is handled by writing each rank to a corresponding indexed file */
    int rank;
    MPI_Comm_rank(comm, &rank);

    filename.insert(filename.find("."), "_" + std::to_string(rank));
    return std::move(filename);
}

template<typename... Args>
void scalpel::record_inputs(MPI_Comm&& comm, Args&&... args)
{
    record_inputs(
        std::ofstream(
            unique_filename_for_process(
                std::forward<MPI_Comm>(comm), "inputs.dat"),
            std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::record_outputs(MPI_Comm&& comm, Args&&... args)
{
    record_outputs(
        std::ofstream(
            unique_filename_for_process(
                std::forward<MPI_Comm>(comm), "outputs.dat"),
            std::ios::binary),
        std::forward<Args>(args)...);
}

/* wrapper for deserialize funcition. Overloaded with serialize */
template<typename... Args>
void scalpel::replay_inputs(MPI_Comm&& comm, Args&&... args)
{
    replay_inputs(
        std::ifstream(
            unique_filename_for_process(
                std::forward<MPI_Comm>(comm), "inputs.dat"),
            std::ios::binary),
        std::forward<Args>(args)...);
}

/* wrapper for compare_output. Overloaded by record outputs functionality */
template<typename... Args>
bool scalpel::compare_outputs(MPI_Comm&& comm, Args&&... args)
{
    return compare_outputs(
        std::ifstream(
            unique_filename_for_process(
                std::forward<MPI_Comm>(comm), "outputs.dat"),
            std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_inputs(MPI_Comm&& comm, Args&&... args)
{
    view_inputs(
        std::ofstream(unique_filename_for_process(
            std::forward<MPI_Comm>(comm), "inputs.txt")),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_outputs(MPI_Comm&& comm, Args&&... args)
{
    view_outputs(
        std::ofstream(unique_filename_for_process(
            std::forward<MPI_Comm>(comm), "outputs.txt")),
        std::forward<Args>(args)...);
}

#endif  // Scalpel_MPI
