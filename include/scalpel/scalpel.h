/*
 * Copyright (c) 2022, Irish Centre for High End Computing (ICHEC), NUI Galway
 * Authors:
 *     Ciarán O'Rourke <ciaran.orourke@ichec.ie>,
 *
 * This file is part of Scalpel.
 *
 * Scalpel is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Scalpel is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Scalpel. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef Scalpel_SCALPEL
#define Scalpel_SCALPEL 1

#include <fstream>
#include <scalpel/parse_variables.h>

namespace scalpel {

/* API */
template<typename... Args>
inline void record_inputs(std::ofstream& stream, Args&&... args);

template<typename... Args>
inline void record_inputs(std::ofstream&& stream, Args&&... args)
{
    record_inputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void record_inputs(Args&&... args);

template<typename... Args>
inline void replay_inputs(std::ifstream& stream, Args&&... args);

template<typename... Args>
inline void replay_inputs(std::ifstream&& stream, Args&&... args)
{
    replay_inputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void replay_inputs(Args&&... args);

template<typename... Args>
inline void record_outputs(std::ofstream& stream, Args&&... args);

template<typename... Args>
inline void record_outputs(std::ofstream&& stream, Args&&... args)
{
    record_outputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void record_outputs(Args&&... args);

template<typename... Args>
bool compare_outputs(std::ifstream& stream, Args&&... args);

template<typename... Args>
bool compare_outputs(std::ifstream&& stream, Args&&... args)
{
    return compare_outputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
bool compare_outputs(Args&&... args);

/* human-readable output functions */
template<typename... Args>
inline void view_inputs(std::ostream& stream, Args&&... args);

template<typename... Args>
inline void view_inputs(std::ostream&& stream, Args&&... args)
{
    view_inputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void view_inputs(std::ofstream& stream, Args&&... args);

template<typename... Args>
inline void view_inputs(std::ofstream&& stream, Args&&... args)
{
    view_inputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void view_inputs(Args&&... args);

template<typename... Args>
inline void view_outputs(std::ostream& stream, Args&&... args);

template<typename... Args>
inline void view_outputs(std::ostream&& stream, Args&&... args)
{
    view_outputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void view_outputs(std::ofstream& stream, Args&&... args);

template<typename... Args>
inline void view_outputs(std::ofstream&& stream, Args&&... args)
{
    view_outputs(stream, std::forward<Args>(args)...);
}

template<typename... Args>
inline void view_outputs(Args&&... args);

}  // namespace scalpel

/*
 * APIs
 * Create stream from provided filename and call appropriate serialization
 * function
 */
template<typename... Args>
void scalpel::record_inputs(Args&&... args)
{
    record_inputs(
        std::ofstream("inputs.dat", std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::record_inputs(std::ofstream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<false>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::record_outputs(Args&&... args)
{
    record_outputs(
        std::ofstream("outputs.dat", std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::record_outputs(std::ofstream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<false>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::replay_inputs(Args&&... args)
{
    replay_inputs(
        std::ifstream("inputs.dat", std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::replay_inputs(std::ifstream& stream, Args&&... args)
{
    scalpel::records log;
    deserialize<false>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
bool scalpel::compare_outputs(Args&&... args)
{
    return compare_outputs(
        std::ifstream("outputs.dat", std::ios::binary),
        std::forward<Args>(args)...);
}

template<typename... Args>
bool scalpel::compare_outputs(std::ifstream& stream, Args&&... args)
{
    scalpel::records log;
    deserialize<true>(log, stream, std::forward<Args>(args)...);

    if (log.get_num_fails() > 0) {
        return false;
    }
    return true;
}

template<typename... Args>
void scalpel::view_inputs(Args&&... args)
{
    view_inputs(std::ofstream("inputs.txt"), std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_inputs(std::ostream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<true>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_inputs(std::ofstream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<true>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_outputs(Args&&... args)
{
    view_outputs(std::ofstream("outputs.txt"), std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_outputs(std::ostream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<true>(log, stream, std::forward<Args>(args)...);
}

template<typename... Args>
void scalpel::view_outputs(std::ofstream& stream, Args&&... args)
{
    scalpel::records log;
    serialize<true>(log, stream, std::forward<Args>(args)...);
}

#endif  // Scalpel_SCALPEL
