\documentclass{beamer}
%\usetheme{Boadilla}

\usetheme{boxes}
\usecolortheme{beaver}
\setbeamercolor{frametitle}{bg=gray!30!white}
\graphicspath{ {/Users/ciaranorourke/learning/cpp/scalpel/doc/images/} }

\usepackage{listings}
\usepackage[T1]{fontenc}
\usepackage[variablett]{lmodern}

\newcommand*{\vttfamily}{%
  \fontencoding{T1}\fontfamily{lmvtt}\selectfont
}

\newcommand*{\textsmallunderscore}{%
  \begingroup
    \fontencoding{T1}\fontfamily{lmtt}\selectfont
    \textunderscore
  \endgroup
}

\renewcommand{\lstlistingname}{Code Block}
\lstset{
  frame=tb,
  language=C++,          
  numbers=none,                     
  numbersep=3pt,
  otherkeywords={1, 2, 3, 4, 5, 6, 7, 8, 9, 0},
  aboveskip=1mm,
  belowskip=1mm,                  
  backgroundcolor=\color{gray!7!white},
  basicstyle=\ttfamily\scriptsize,
  numberstyle=\tiny\color{red},
  keywordstyle=\color{red!60!black}\ttfamily,
  commentstyle=\color{blue!50!black}\ttfamily,
  literate={_}{\textsmallunderscore}1,
  showspaces=false,          
  showstringspaces=false,      
  showtabs=false,             
  columns=fullflexible,
  tabsize=4,              
  captionpos=b,      
  breaklines=true,     
  breakatwhitespace=true,              
}

\def\mysingleq#1{`#1'}

\setbeamertemplate{navigation symbols}{}

\title[Lattice QFT]{Scalpel}

\author{Ciar\'an O' Rourke}

\institute{ICHEC}

\date{\today}

\begin{document}

%\begin{frame}
%\frametitle{Outline}
%\tableofcontents
%\end{frame}

\section{Motivation}

\section{Usage}
\begin{frame}[fragile]
\frametitle{Typical Usage}

\begin{lstlisting}[title={Project source code.},label={code:project},captionpos=b, escapechar=\%]
#include "scalpel.hpp"
%$\cdots$%
scalpel::inputs(scalpel::record, "inputs.dat", x, a, phi, f_array, d_vector);
//
// calculation
//
scalpel::outputs(scalpel::record, "outputs.dat", x, a, phi, f_array, d_vector);
%$\cdots$%
\end{lstlisting}

\begin{lstlisting}[title={Script with code block to optimize.},label={code:optimize},captionpos=b, escapechar=\%]
#include "scalpel.hpp"
%$\cdots$%
// variable declarations
scalpel::inputs(scalpel::replay, "inputs.dat", x, a, phi, f_array, d_vector);
//
// optimized calculation
//
scalpel::outputs(scalpel::compare, "outputs.dat", x, a, phi, f_array, d_vector);
\end{lstlisting}

\end{frame}

\section{General practicalities}

\begin{frame}[fragile]
\frametitle{Variable Number of Arguments}

\begin{lstlisting}[title={Dealing with variable numbers of arguments.},label={code:variadic},captionpos=b, escapechar=\%]
template<typename T> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const T& var) {
	%$\cdots$%
}
template<typename T, typename... Args> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const T& var, const Args&... args) {
	%$\cdots$%
	process_next_variable(is_textfile, write_file, args...);
}
\end{lstlisting}

\begin{itemize}
\item A combination of \structure{templates}, \structure{overloading}, and \structure{recursion} allows for isolation of logically related arguments.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{IO: Serialization}

\begin{lstlisting}[title={Writing of Final Variable.},label={code:process},captionpos=b, escapechar=\%]
template<typename T> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const T& var) {
    if (is_textfile) {
        int ret = record_variable_viewable(write_file, var);
        /* endline to separate variables */
        write_file << std::endl;
        return ret;
    }
    else {
        return record_variable(write_file, var);
    }
}
\end{lstlisting}

\begin{itemize}
\item Option to write to text file for manual viewing.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{IO: Serialization}

\begin{lstlisting}[title={Variable writing switch.},label={code:record},captionpos=b, escapechar=\%]
template<typename T> int scalpel::record_variable(std::ofstream& write_file, const T& var) {
    const int size = sizeof(T);
    switch (size) {
        case 1:
            return record_variable_8(write_file, var);
        case 2:
            return record_variable_16(write_file, var);
        case 4:
            return record_variable_32(write_file, var);
        case 8:
            return record_variable_64(write_file, var);
        default:
            return 1;
    }   
}
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{IO: Serialization}

\begin{lstlisting}[title={Write Variable of Length 64.},label={code:record_var},captionpos=b, escapechar=\%]
template<typename T> int scalpel::record_variable_64(std::ofstream& write_file, const T& var) {
    const int num_bytes = sizeof(T);
    uint8_t bytes[num_bytes];
    uint64_t bits;
    memcpy(&bits, &var, num_bytes);
    for(int i = 0; i < num_bytes; i++) {
        bytes[i] = ((bits >> 8*i) & 0xFF);
    }
    write_file.write((char*)bytes, num_bytes);
    return 0;
}
\end{lstlisting}

\begin{itemize}
\item Place successive bytes in `byte' array.
	\begin{itemize}
	\item $\Rightarrow$ Abstracts byte order away from \structure{endianness}.
	\end{itemize}
\item Bitshift appropriate byte into \structure{least significant} byte of data.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{IO: Deserialization}

\begin{lstlisting}[title={Read Variable of Length 64.},label={code:set},captionpos=b, escapechar=\%]
template<typename T> int scalpel::set_variable_64(std::ifstream& read_file, T& var) {
    const int num_bytes = sizeof(T);
    uint8_t bytes[num_bytes];
    uint64_t bits = 0;
    read_file.read((char*)bytes, num_bytes);
    for (int i = 0; i < num_bytes; i++) {
        bits += ((uint64_t)bytes[i] << 8*i);
    }   
    memcpy(&var, &bits, num_bytes);
    return 0;
}

\end{lstlisting}

\begin{itemize}
\item Read data back into \structure{uint8\_t} array. 
\item Add each byte's shifted integer value to a running total.
	\begin{itemize}
	\item Those bits are copied into a variable of the desired type.
	\end{itemize}
\end{itemize}

\end{frame}

\section{Niche support}

\begin{frame}[fragile]
\frametitle{Array and Vector Support}

\begin{lstlisting}[title={Write Vector of Type T to File.},label={code:vector},captionpos=b, escapechar=\%]
template<typename T> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const std::vector<T>& var) {
    if (is_textfile) {
        %$\cdots$%
    }
    else {
        for (int i = 0; i < var.size(); i++) {
            if (record_variable(write_file, var[i])) {
                return 1;
            }
        }
    }
    return 0;
}
\end{lstlisting}

\begin{itemize}
\item Pass writing responsibility to templated \structure{record\_variable} function
	\begin{itemize}
	\item $\Rightarrow$ Each element is written individually.
	\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Array and Vector Support}

\begin{lstlisting}[title={Write Vector of Type T to File.},label={code:over_vector},captionpos=b, escapechar=\%]
template<typename T> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const std::vector<T>& var) {
	%$\cdots$%
}
template<typename T, typename... Args> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const std::vector<T>& var, const Args&... args) {
	%$\cdots$%
	return process_next_variable(is_textfile, write_file, args...);
}
\end{lstlisting}

\begin{itemize}
\item Like before, we need to overload for subsequent variable arguments.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Array and Vector Support: Arbitrary Number of Dimensions}

\begin{lstlisting}[title={Write Arbitrary Dimensional Vector to File.},label={code:ad_vector},captionpos=b, escapechar=\%]
template<typename T> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const std::vector< std::vector<T> >& var) {
    for (int i = 0; i < var.size(); i++) {
        if (process_next_variable(is_textfile, write_file, var[i])) {
            return 1;
        }
    }
    return 0;
}
template<typename T, typename... Args> int scalpel::process_next_variable(bool is_textfile, std::ofstream& write_file, const std::vector< std::vector<T> >& var, const Args&... args) {
    for (int i = 0; i < var.size(); i++) {
        if (process_next_variable(is_textfile, write_file, var[i])) {
            return 1;
        }
    }
    return process_next_variable(is_textfile, write_file, args...);
}
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{MPI Support: Usage}
\begin{lstlisting}[title={Project source code.},label={code:mpi_proj},captionpos=b, escapechar=\%]
#include "scalpel.hpp"
#include <mpi.h>
%$\cdots$%
scalpel::inputs(MPI_COMM_WORLD, comm, scalpel::record, "inputs.dat", x, a, phi, f_array, d_vector);
// calculation
scalpel::outputs(MPI_COMM_WORLD, scalpel::record, "outputs.dat", x, a, phi, f_array, d_vector);
%$\cdots$%
\end{lstlisting}

\begin{lstlisting}[title={Script with code block to optimize.},label={code:mpi_opti},captionpos=b, escapechar=\%]
#include "scalpel.hpp"
#include <mpi.h>
%$\cdots$%
// variable declarations
// MPI setup
scalpel::inputs(MPI_COMM_WORLD, scalpel::replay, "inputs.dat", x, a, phi, f_array, d_vector);
// optimized calculation
scalpel::outputs(MPI_COMM_WORLD, scalpel::compare, "outputs.dat", x, a, phi, f_array, d_vector);
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{MPI Support}
\begin{lstlisting}[title={Project source code.},label={code:mpi_wrapper},captionpos=b, escapechar=\%]
template<typename... Args> int scalpel::inputs(MPI_Comm comm, scalpel::record_type record, std::string filename, const Args&... args) {
    int ret = serialize(comm, false, filename, args...);
    %$\cdots$%

}
template<typename... Args> int scalpel::outputs(MPI_Comm comm, scalpel::record_type record, std::string filename, const Args&... args) {
    int ret = serialize(comm, false, filename, args...);
    %$\cdots$%
}
template<typename... Args> int scalpel::outputs(MPI_Comm comm, scalpel::view_type record_view, std::string filename, const Args&... args) {
    int ret = serialize(comm, true, filename, args...);
    %$\cdots$%
}
\end{lstlisting}

\begin{itemize}
\item Overload wrapper functions with \structure{MPI communicator} argument.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{MPI Support}
\begin{lstlisting}[title={MPI serialization.},label={code:mpi_filenames},captionpos=b, escapechar=\%]
template<typename... Args> int scalpel::serialize(MPI_Comm comm, bool is_textfile, std::string filename, const Args&... args) {
    int ret;
    /* MPI is handled by writing each rank to a corresponding indexed file */
    int rank;
    MPI_Comm_rank(comm, &rank);
    std::string index = std::to_string(rank);
    std::size_t dot = filename.find_first_of('.');
    filename.insert(dot, index);
    std::ofstream write_file;
    %$\cdots$%
    ret = process_next_variable(is_textfile, write_file, args...);
    write_file.close();
    return ret;
}

\end{lstlisting}

\begin{itemize}
\item Each MPI process write to its own file.
\item Input filename is parsed and appended with process \structure{rank}.
\end{itemize}


\end{frame}

\end{document}