add_library(development_flags INTERFACE)

option (SCALPEL_ENABLE_DEBUG_FLAGS "Enable a variety of Debug flags." ON)
option (SCALPEL_ENABLE_SANITIZERS  "Enable ASAN and UBSAN." ON)

if(CMAKE_CXX_COMPILER_ID MATCHES "Clang|GNU")
    if (SCALPEL_ENABLE_DEBUG_FLAGS)
        target_compile_options(
            development_flags
            INTERFACE
                -Wall -Wextra -Wpedantic -pedantic
                -Wno-error=unknown-pragmas
                -Wswitch-enum
                -Wimplicit-fallthrough
                -Werror
        )
    endif (SCALPEL_ENABLE_DEBUG_FLAGS)

    if (SCALPEL_ENABLE_COVERAGE)
        target_compile_options(
            development_flags
            INTERFACE
                --coverage
        )
    elseif(SCALPEL_ENABLE_SANITIZERS)
            target_compile_options(
                development_flags
                INTERFACE
                    -fsanitize=address,undefined
                    -fno-sanitize-recover=all
                    -fno-omit-frame-pointer
            )
            target_link_libraries(
                development_flags
                INTERFACE
                    -fsanitize=address,undefined
            )

            if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
                target_compile_options(development_flags INTERFACE -ferror-limit=1)
            endif(CMAKE_CXX_COMPILER_ID MATCHES "Clang")

            if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
                target_compile_options(development_flags INTERFACE -fmax-errors=1)
            endif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    endif (SCALPEL_ENABLE_COVERAGE)
endif()
