# Scalpel

Scalpel is an optimisation facilitation tool. It allows for the extraction of code blocks from large projects in order to be run in an isolated environment. The requires the entire project to be run once in order to record the inputs to the code block and results of the code block for comparison with optimised code output. 

## Building Instructions

To configure and build Scalpel, a C++11 compatible compiler is needed, MPI, and CMake >= 3.10).
    
    # Make a directory to hold the temporary build files
    mkdir -p /path/to/build/directory

    # Change directory to the build directory
    cd /path/to/build/directory

    # Configure the Scalpel build using CMake
    cmake \
        -DCMAKE_INSTALL_PREFIX=/path/to/install/directory \
        /path/to/the/scalpel/project

    # Build the Scalpel project
    make
    
    # Test the Scalpel project
    make test 

    # Install the Scalpel project
    make install
    
    # (Optional) clean up all the temporary build files
    # cd $(HOME)
    # rm -rf /path/to/build/directory

CMake should fing the necessary MPI library automatically. It also searches for the C++ compiler in the CXX environment variable.

### Useful Options

#### Setting Compile/Link Flags
Option | Effect
------ | ------
`-DCMAKE_CXX_COMPILER=...` | Set the C++ compiler.
`-DCMAKE_CXX_FLAGS=...`    | Set the flags to pass to the C++ compiler. Overrides the default flags.

#### Dependencies
Option | Effect
------ | ------
`-DMPI_CXX_COMPILER=...` | Set the MPI C++ compiler wrapper (i.e. mpicxx).

#### Enabling / Disabling Sections of Scalpel 
Option | Effect
------ | ------
`-DSCALPEL_BUILD_EXAMPLES=...`      | Set to `ON` to build Scalpel example programs or `OFF` to skip (Default `ON`).
`-DSCALPEL_BUILD_TESTS=...`         | Set to `ON` to build Scalpel tests and enable the `make test` target, or `OFF` to skip (Default `ON`).
`-DSCALPEL_ENABLE_MPI=...`          | Set to `ON` to include MPI examples and tests (Default `OFF`).
`-DSCALPEL_ENABLE_COVERAGE=...`     | Set to `ON` to build with code coverage capabliites. Note: must be built in `Debug` mode (Default `OFF`).

#### Installation Directories
Option | Effect
------ | ------
`-DCMAKE_INSTALL_PREFIX=...` | Set the root install directory for the compiled libraries and programs.
`-DCMAKE_INSTALL_BINDIR=...` | Set the install directory for Scalpel executables. Use a relative path to set the path relative to `${CMAKE_INSTALL_PREFIX}`. (Default `bin`)

## Usage

To avail of this functionality outside of this code base after installation, "scalpel.h" should be included in the file containing the code block to be optimized. 

Given as set of input parameters to the code block, a serialisation function should be called immediately before entering the codeblock to record the inputs. Similarly, another serialisation function should be invoked immediately following the codeblock to record relevant output parameters. This is done as follows;
```
scalpel::record_inputs(par1, par2, par3, ..., parN);
...
/* calculation */
...
scalpel::record_outputs(par1, par2, par3, ..., parN); 
```

An isolated .cpp file, also with the "scalpel.h" header should be constructed as follows in order to optimise the code block;
```
/* variable declarations */
scalpel::replay_inputs(par1, par2, par3, ..., parN);
...
/* calculation */
...
scalpel::compare_outputs(par1, par2, par3, ..., parN); 
```

The last function determines whether the output parameters from the altered code block match the outputs from the original.

```
scalpel::view_<inputs/outputs>(<optional> std::ostream, par1, par2, ..., parN); 
```

may be used to inspect variables in human-readable format. Supply `std::cout` as
the first argument to simply print to stdout.

## Features

Features currently supported are;
- (arbitrary-dimensional) std::array as parameters
- (arbitrary-dimensional) std::vector as parameters
- Passing scalpel::record_view as the first parameter writes in human-readable format
- MPI - simply supply an MPI communicator as the first parameter to each inputs/outputs function call. Each processor will write to its own file.

Examples of each functionality can be seen in the examples directory.

# License

Scalpel is licensed under the [GPL-3.0-or-later](LICENSE) license.

Copyright (c) 2022 Irish Centre for High-End Computing (ICHEC), NUI Galway
